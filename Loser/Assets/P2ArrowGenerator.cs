﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2ArrowGenerator : MonoBehaviour
{
    public GameObject P2ArrowPrefab;
    float delta = 0;

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;


        if (Input.GetKeyDown(KeyCode.D))
        {
            if (this.delta >= 1.0f)
            {
                this.delta--;
                GameObject go = Instantiate(P2ArrowPrefab) as GameObject;
                go.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
            }
        }
    }
}
