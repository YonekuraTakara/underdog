﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameDirector : MonoBehaviour
{
    // Start is called before the first frame update

    public static int P1 = 0;
    public static int P2 = 0;
    public static int Win = 0;
    public GameObject P1Gauge;
    public GameObject P2Gauge;

    void Start()
    {
        this.P1Gauge = GameObject.Find("P1Gauge");
        this.P2Gauge = GameObject.Find("P2Gauge");
        P1 = 0;
        P2 = 0;
        Win = 0;

    }

    public void DecreaseP1()
    {
        this.P1Gauge.GetComponent<Image>().fillAmount -= 0.1f;
        P1++;
    }

    public void DecreaseP2()
    {
        this.P2Gauge.GetComponent<Image>().fillAmount -= 0.1f;
        P2++;
    }

    void Update()
    {
        
        //ゲームオーバー判定
        if (P1 >= 10)
        {
            SceneManager.LoadScene("GameOverScene");
            Win = 1;
        }

        if (P2 >= 10)
        {
            SceneManager.LoadScene("GameOverScene");
            Win = 2;
        }
    }

    public static int getScore()
    {
        return Win;
    }
}