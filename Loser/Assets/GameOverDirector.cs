﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverDirector : MonoBehaviour
{
    public GameObject Player1;
    public GameObject Player2;


    // Start is called before the first frame update
    void Start()
    {
        int Win = GameDirector.getScore();

        if (Win == 1)
        {
            GameObject go = Instantiate(Player1) as GameObject;
            go.transform.position = new Vector3(7, 0, 0);
        }

        if (Win == 2)
        {
            GameObject go = Instantiate(Player2) as GameObject;
            go.transform.position = new Vector3(-7, 0, 0);
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
