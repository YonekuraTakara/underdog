﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2ArrowController : MonoBehaviour
{
    GameObject Player1;

    // Start is called before the first frame update
    void Start()
    {
        this.Player1 = GameObject.Find("Player1");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0.5f, 0, 0);

        if (transform.position.x > 15.0f)
        {
            Destroy(gameObject);
        }

        Vector2 b1 = transform.position;
        Vector2 b2 = this.Player1.transform.position;
        Vector2 bth = b1 - b2;
        float t = bth.magnitude;
        float h1 = 0.5f;
        float h2 = 1.0f;

        if (t < h1 + h2)
        {
            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseP2();
            Destroy(gameObject);
        }
    }
}
