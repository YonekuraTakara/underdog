﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < 4.5f)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(0, 0.2f, 0);
            }
        }

        if (transform.position.y > -4.5f)
        {
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(0, -0.2f, 0);
            }
        }
    }
}
