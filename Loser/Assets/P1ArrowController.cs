﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P1ArrowController : MonoBehaviour
{
    GameObject Player2;

    // Start is called before the first frame update
    void Start()
    {
        this.Player2 = GameObject.Find("Player2");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-0.5f, 0, 0);

        if (transform.position.x < -15.0f)
        {
            Destroy(gameObject);
        }

        Vector2 p1 = transform.position;
        Vector2 p2 = this.Player2.transform.position;
        Vector2 dir = p1 - p2;
        float d = dir.magnitude;
        float r1 = 0.5f;
        float r2 = 1.0f;

        if (d < r1 + r2)
        {
            GameObject Director = GameObject.Find("GameDirector");
            Director.GetComponent<GameDirector>().DecreaseP1();
            Destroy(gameObject);
        }
    }
}
